from .base import *

DEBUG = env('DEBUG')
SECRET_KEY = env('SECRET_KEY')

ADMINS = (
    ('Frankline Ogongi', 'fraogongi@gmail.com'),
)

ALLOWED_HOSTS = ['myproject.com', 'www.myproject.com']

DATABASES = {
    'default': env.db(),
}
