# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-19 15:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('description', models.TextField()),
                ('start_time', models.DateTimeField(blank=True, null=True, verbose_name='start')),
                ('end_time', models.DateTimeField(help_text='The end time must be later than the start time.', verbose_name='end')),
            ],
            options={
                'ordering': ('start_time',),
            },
        ),
    ]
