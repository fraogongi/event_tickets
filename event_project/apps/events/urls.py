from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^mine/$', views.ManageEventListView.as_view(), name='manage_event_list'),
    url(r'^create/$', views.EventCreateView.as_view(), name='event_create'),
    url(r'^(?P<pk>\d+)/edit/$', views.EventUpdateView.as_view(), name='event_edit'),
    url(r'^(?P<pk>\d+)/delete/$', views.EventDeleteView.as_view(), name='event_delete'),
    url(r'^(?P<pk>\d+)/$', views.EventDetailView.as_view(), name='event_detail'),
]