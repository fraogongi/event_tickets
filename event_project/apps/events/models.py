from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Event(models.Model):
    owner = models.ForeignKey(User, related_name='events_created')
    name = models.CharField(_("event name"), max_length=150)
    description = models.TextField(blank=True)
    start_time = models.DateTimeField(_("start date"), blank=True, null=True)
    end_time = models.DateTimeField(_("end date"), help_text=_("The end time must be later than the start time."))

    class Meta:
        ordering = ('start_time',)
    
    def __str__(self):
        return self.name
    

class TicketType(models.Model):
    """A category or type of ticket specific to an event."""
    event = models.ForeignKey(Event, related_name='ticket_types')
    name = models.CharField(_("ticket name"), max_length=150)
    description = models.TextField(blank=True)
    quantity = models.IntegerField(_("quantity available"), default=0,
                                   help_text=_('Number of tickets which can be issued. For unlimited amount use -1.'))
    price = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(0)])
    # Free Ticket, Paid Ticket, Donation
    # See: https://www.eventbrite.com/support/articles/en_US/How_To/how-to-create-custom-ticket-types?lg=en_US

    def __str__(self):
        return self.name


class TicketPurchase(models.Model):
    AWAITING = 0
    PAID = 1
    PARTLY_PAID = 2
    EXPIRED = 3
    CANCELLED = 4

    ORDER_CHOICES = (
        (AWAITING, _('Awaiting payment')),
        (PARTLY_PAID, _('Partly paid')),
        (PAID, _('Paid')),
        (EXPIRED, _('Expired')),
        (CANCELLED, _('Cancelled')),
    )

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. "
                                         "Up to 15 digits allowed.")

    name = models.CharField(max_length=100)
    email = models.EmailField(null=False, blank=False, verbose_name="email address")
    phone_number = models.CharField(null=False, blank=False, max_length=15, validators=[phone_regex],
                                    help_text=_('Phone number e.g. +2547000000000'))
    status = models.PositiveSmallIntegerField(choices=ORDER_CHOICES, default=AWAITING, blank=False, null=False)
    amount_paid = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(0)], default=0)
    purchase_date = models.DateTimeField(auto_now_add=True)
    payment_date = models.DateTimeField(blank=True, null=True)
    # discount(promo), purchase_date, payment_date

    def __str__(self):
        return self.email


class Ticket(models.Model):
    """a ticket"""
    event = models.ForeignKey(Event, related_name="tickets")
    ticket_type = models.ForeignKey(TicketType)
    purchase = models.ForeignKey(TicketPurchase, related_name="tickets")

    def __str__(self):
        return self.purchase.name
