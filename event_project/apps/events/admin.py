from django.contrib import admin
from .models import Event, TicketType


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ['name', 'start_time', 'end_time']
    list_filter = ['start_time']
    search_fields = ['name']


@admin.register(TicketType)
class TicketTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'event', 'quantity', 'price']
