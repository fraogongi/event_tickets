from braces.views import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from .models import Event


class OwnerMixin(object):

    def get_queryset(self):
        qs = super(OwnerMixin, self).get_queryset()
        return qs.filter(owner=self.request.user)


class OwnerEditMixin(object):

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(OwnerEditMixin, self).form_valid(form)


class OwnerEventMixin(LoginRequiredMixin, OwnerMixin):
    model = Event
    fields = ['name', 'start_time']
    success_url = reverse_lazy('manage_event_list')


class OwnerEventEditMixin(OwnerEventMixin, OwnerEditMixin):
    fields = ['name', 'description', 'start_time', 'end_time']
    success_url = reverse_lazy('manage_event_list')
    template_name = 'events/manage/event/form.html'


class ManageEventListView(OwnerEventMixin, ListView):
    template_name = 'events/manage/event/list.html'


class EventCreateView(PermissionRequiredMixin, OwnerEventEditMixin, CreateView):
    permission_required = 'events.add_event'


class EventUpdateView(PermissionRequiredMixin, OwnerEventEditMixin, UpdateView):
    template_name = 'events/manage/event/form.html'
    permission_required = 'events.change_event'


class EventDeleteView(PermissionRequiredMixin, OwnerEventMixin, DeleteView):
    template_name = 'events/manage/event/delete.html'
    success_url = reverse_lazy('manage_event_list')
    permission_required = 'events.delete_event'


class EventListView(TemplateResponseMixin, View):
    model = Event
    template_name = 'events/event/list.html'
    
    def get(self, request):
        events = Event.objects.all()
        return self.render_to_response({'events': events})
    

class EventDetailView(DetailView):
    model = Event
    template_name = 'events/event/detail.html'
